git help = mostra comando que podem te auxiliar de forma resumida.
git help <comando> = mostra de forma mais específica o comando especificado.
git config --global user.name “Seu Nome” = Define seu nome de usuário no git.
git config --global user.email “Seu email” = Define seu email no git.
git init = Dá inicio ao repositório git.
git clone <origem> = Clona repositório para a pasta que selecionou.
git push = envia as mudanças comitadas para branch em questão.
git pull = atualiza o repositório locol com os dados do repositório remoto.
git merge <nome da branch> = Mescla as mudanças presentes na branch selecionada na branch corrente.
git branch = Lista as branchs locais.
git status = Verifica os estados dos arquivos do repositório.
git log = mostra as alterações feitas em ordem cronológica.

